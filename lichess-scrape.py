import berserk
import csv
from datetime import datetime

client = berserk.Client()

start = berserk.utils.to_millis(datetime(2021, 1, 1))
result = client.games.export_by_player('mrnohr', since=start)
game_list = list(result)

game_counts = []

# print(game_list[1])

for next_game in game_list:
    date = next_game['lastMoveAt']
    short_date = date.strftime("%b %d, %Y") # Match the format needed for CSV file
    points = 0
    if next_game['speed'] == 'correspondence':
        # Treat daily games the same as one 5-minute blitz game
        points = 300
    else:
        points = int(next_game['clock']['totalTime'])

     # Are there already any games for this date?
    points_per_day = None
    # print(game_counts)
    for x in game_counts:
        if x['date'] == short_date:
            points_per_day = x
            break
    if points_per_day is None:
        # First game on this date, initialze
        points_per_day = {'date': short_date, 'points': points}
        game_counts.append(points_per_day)
    else:
        points_per_day['points'] += points

    # print(f"{short_date} - {points}")

header = ['date', 'points']
with open('lichess.csv', 'w', encoding='UTF8') as f:
    writer = csv.writer(f)
    writer.writerow(header)

    for next_date in game_counts:
        writer.writerow([next_date['date'], next_date['points']])