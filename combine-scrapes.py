from datetime import datetime
import csv  

combined_stats = []

def read_stats_from_file(current_stats, file_name):
    with open(file_name, newline='') as chessable_file:
        csvreader = csv.reader(chessable_file, delimiter=',')
        skipFirst = True
        for row in csvreader:
            if skipFirst:
                skipFirst = False
            else:
                points = int(row[1].replace(',', ''))
                date = datetime.strptime(row[0], '%b %d, %Y')
                
                
                # Are there already any games for this date?
                points_per_day = None
                for x in current_stats:
                    if x['date'] == date:
                        points_per_day = x
                        break
                if points_per_day is None:
                    # First game on this date, initialze
                    points_per_day = {'date': date, 'points': points}
                    current_stats.append(points_per_day)
                else:
                    points_per_day['points'] += points

# read_stats_from_file(combined_stats, 'chessable.csv')
read_stats_from_file(combined_stats, 'chesscom.csv')
read_stats_from_file(combined_stats, 'lichess.csv')

sorted_stats = sorted(combined_stats, key = lambda i: i['date'])

header = ['date', 'points']
with open('combined.csv', 'w', encoding='UTF8') as f:
    writer = csv.writer(f)
    writer.writerow(header)

    for next_stat in sorted_stats:
        short_date = next_stat["date"].strftime('%b %d, %Y')
        writer.writerow([short_date, next_stat['points']])
