import requests
from datetime import datetime
import calendar
import csv  

USER_NAME = 'mrnohr'
lookup_date = datetime(2021, 1, 1)

def add_months(sourcedate, months):
    month = sourcedate.month - 1 + months
    year = sourcedate.year + month // 12
    month = month % 12 + 1
    day = min(sourcedate.day, calendar.monthrange(year,month)[1])
    return datetime(year, month, day)

game_counts = []

while lookup_date < datetime.now():
    year = lookup_date.year
    month = str(lookup_date.month).rjust(2, '0')
    api = f"https://api.chess.com/pub/player/{USER_NAME}/games/{year}/{month}"
    print(api)

    r = requests.get(api)
    if 'games' in r.json():
        game_list = r.json()['games']
        for next_game in game_list:
            date = datetime.fromtimestamp(next_game['end_time'])
            short_date = date.strftime("%b %d, %Y") # Match the format needed for CSV file
            points = 0
            if next_game['time_class'] == 'daily':
                # Treat daily games the same as one 5-minute blitz game
                points = 300
            elif '+' in next_game['time_control']:
                # Game with increment
                moves_per_game = 40
                parts = next_game['time_control'].split('+')
                # base + (moves * increment)
                points = int(parts[0]) + (moves_per_game * int(parts[1]))
            else:
                # Game with no increment
                points = int(next_game['time_control'])

            # Are there already any games for this date?
            points_per_day = None
            # print(game_counts)
            for x in game_counts:
                if x['date'] == short_date:
                    points_per_day = x
                    break
            if points_per_day is None:
                # First game on this date, initialze
                points_per_day = {'date': short_date, 'points': points}
                game_counts.append(points_per_day)
            else:
                points_per_day['points'] += points
    else:
        print(f"No games found for {lookup_date}")
        print(r.json())
        
    lookup_date = add_months(lookup_date, 1)


# for next_date in game_counts:
#     print(next_date)

header = ['date', 'points']
with open('chesscom.csv', 'w', encoding='UTF8') as f:
    writer = csv.writer(f)
    writer.writerow(header)

    for next_date in game_counts:
        writer.writerow([next_date['date'], next_date['points']])