import requests
import csv  
from bs4 import BeautifulSoup


profile = "MattPlaysChess"
URL = "https://www.chessable.com/ajax/timemap.php?name=" + profile + "&days=365&h=85dc52eaec2b97c5d4e93c11cce09605"
headers = {'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36'}
print(URL)
page = requests.get(URL, headers=headers)

soup = BeautifulSoup(page.content, "html.parser")
tooltips = soup.find_all("div", class_="timemap__tooltip")

header = ['date', 'points']
with open('chessable.csv', 'w', encoding='UTF8') as f:
    writer = csv.writer(f)
    writer.writerow(header)

    for tooltip in tooltips:
        text = tooltip.text
        parts = text.split()
        points = parts[0]
        date = " ".join(parts[3:6])

        writer.writerow([date, points])